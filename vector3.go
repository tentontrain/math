package math

import (
	"math"
)

type Vector3[N Number] struct{ X, Y, Z N }

func (v Vector3[N]) Zero() Vector3[N] {
	return Vector3[N]{0, 0, 0}
}

func (v Vector3[N]) Add(b Vector3[N]) Vector3[N] {
	return Vector3[N]{v.X + b.X, v.Y + b.Y, v.Z + b.Z}
}

func (v Vector3[N]) Sub(b Vector3[N]) Vector3[N] {
	return Vector3[N]{v.X - b.X, v.Y - b.Y, v.Z - b.Z}
}

func (v Vector3[N]) Mul(b Vector3[N]) Vector3[N] {
	return Vector3[N]{v.X * b.X, v.Y * b.Y, v.Z * b.Z}
}

func (v Vector3[N]) Div(b Vector3[N]) Vector3[N] {
	return Vector3[N]{v.X / b.X, v.Y / b.Y, v.Z / b.Z}
}

func (v Vector3[N]) Dot(b Vector3[N]) N {
	return v.X*b.X + v.Y*b.Y + v.Z*b.Z
}

func (v Vector3[N]) Scale(b N) Vector3[N] {
	return Vector3[N]{v.X * b, v.Y * b, v.Z * b}
}

func (v Vector3[N]) Length() N {
	return N(math.Sqrt(float64(v.X*v.X) + float64(v.Y*v.Y) + float64(v.Z*v.Z)))
}

func (v Vector3[N]) Equals(b Vector3[N], e N) bool {
	return Equals(v.X, b.X, e) && Equals(v.Y, b.Y, e) && Equals(v.Z, b.Z, e)
}

// Normalize returns a unit vector with the same direction as v. Will not work on integers
func (v Vector3[N]) Normalize() Vector3[N] {
	length := v.Length()
	return Vector3[N]{v.X / length, v.Y / length, v.Z / length}
}

func Foo[T float32 | float64](a Vector3[T]) {

}
