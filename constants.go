package math

const (
	EpsilonLax    = 0.001
	Epsilon       = 0.00001
	EpsilonStrict = 0.00000001
)
