// Operations that work on integer points

package math

// Is a diagonal to b
// [ ][A] or [A][ ]
// [B][ ]    [ ][B]
func Diagonal(a, b Vector2[int]) bool {
	return (Abs(a.X-b.X) == Abs(a.Y-b.Y))
}

// Scale vector's length to be l (in Chebyshev distance)
func ScaleChess(v Vector2[int], l int) Vector2[int] {
	len := Max(Abs(v.X), Abs(v.Y))
	if len == 0 {
		return v
	}
	v.X = v.X * l / len
	v.Y = v.Y * l / len
	return v
}
