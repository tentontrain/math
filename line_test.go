package math

import "testing"

func TestLine(t *testing.T) {

	line := Line(Vector2[int]{0, 0}, Vector2[int]{2, -3})
	res := []Vector2[int]{
		{0, 0}, {1, -1}, {1, -2}, {2, -3},
	}

	if len(line) != len(res) {
		t.Error(len(line), len(res))
		t.FailNow()
	}

	for i := range res {
		if res[i] != line[i] {
			t.Error(res[i], line[i])
		}
	}

	tests := []struct {
		p0, p1 Vector2[int]
	}{
		{Vector2[int]{0, 0}, Vector2[int]{2, 3}},
		{Vector2[int]{0, 0}, Vector2[int]{2, -3}},
		{Vector2[int]{0, 0}, Vector2[int]{-2, 3}},
		{Vector2[int]{0, 0}, Vector2[int]{-2, -3}},
		{Vector2[int]{0, 0}, Vector2[int]{-5, -3}},
		{Vector2[int]{0, 0}, Vector2[int]{-5, 3}},
	}

	for i := range tests {
		l := Line(tests[i].p0, tests[i].p1)
		if l[0] != tests[i].p0 || l[len(l)-1] != tests[i].p1 {
			t.Error(l, tests[i])
		}
	}

}

func TestSymLine(t *testing.T) {

	t.Log(SymLine(Vector2[int]{0, 0}, Vector2[int]{-2, -3}))

	tests := []struct {
		p0, p1 Vector2[int]
	}{
		{Vector2[int]{-1, -2}, Vector2[int]{2, 3}},
		{Vector2[int]{-2, 2}, Vector2[int]{2, -3}},
		{Vector2[int]{-1, -1}, Vector2[int]{-2, 3}},
		{Vector2[int]{0, 0}, Vector2[int]{-2, -3}},
		{Vector2[int]{1, 1}, Vector2[int]{-5, -3}},
		{Vector2[int]{5, 2}, Vector2[int]{-5, 3}},
		{Vector2[int]{0, 0}, Vector2[int]{-1, 1}},
		{Vector2[int]{0, 0}, Vector2[int]{1, 1}},
		{Vector2[int]{0, 0}, Vector2[int]{1, -1}},
	}

	for i := range tests {
		l := SymLine(tests[i].p0, tests[i].p1)
		if l[0] != tests[i].p0 || l[len(l)-1] != tests[i].p1 {
			t.Error(l, tests[i])
		}
	}
}

func BenchmarkSymLine(b *testing.B) {

	for n := 0; n < b.N; n++ {
		SymLine(Vector2[int]{0, 2336}, Vector2[int]{4561, -4562})
	}
}

func BenchmarkLine(b *testing.B) {

	for n := 0; n < b.N; n++ {
		Line(Vector2[int]{0, 2336}, Vector2[int]{4561, -4562})
	}
}

func TestGridLine(t *testing.T) {

	tests := []struct {
		a, b Vector2[int]
		l    int
	}{
		{Vector2[int]{0, 0}, Vector2[int]{0, 0}, 5},
		{Vector2[int]{1, 1}, Vector2[int]{4, 3}, 5},
		{Vector2[int]{1, 1}, Vector2[int]{4, 1}, 5},
		{Vector2[int]{1, 1}, Vector2[int]{2, 4}, 5},
		{Vector2[int]{1, 1}, Vector2[int]{4, 0}, 5},
		{Vector2[int]{1, 2}, Vector2[int]{4, -1}, 5},
		{Vector2[int]{3, 3}, Vector2[int]{1, 2}, 5},
		{Vector2[int]{1, 2}, Vector2[int]{4, -1}, 5},
		{Vector2[int]{0, 0}, Vector2[int]{0, -1}, 2},
	}

	for _, v := range tests {
		line := GridLine(v.a, v.b, v.l)
		if v.b != line[len(line)-1] || v.a != line[0] {
			t.Error(v, line, len(line))
		}
	}

	grid := make([][]int, 9)
	for i := range grid {
		grid[i] = make([]int, 9)
	}

	targets := []Vector2[int]{}
	for i := 0; i < 9; i++ {
		targets = append(targets, Vector2[int]{i, 0})
	}
	for i := 1; i < 9; i++ {
		targets = append(targets, Vector2[int]{8, i})
	}
	for i := 7; i >= 0; i-- {
		targets = append(targets, Vector2[int]{i, 8})
	}
	for i := 7; i > 0; i-- {
		targets = append(targets, Vector2[int]{0, i})
	}

	for _, v := range targets {
		points := GridLine(Vector2[int]{4, 4}, v, 99)

		if points[len(points)-1] != v {
			t.Error("did not reach target ", v)
		}
	}

	if testing.Verbose() {
		t.Log(GridLine(Vector2[int]{0, 0}, Vector2[int]{-2, -2}, 3))
		t.Log(GridLine(Vector2[int]{0, 0}, Vector2[int]{-1, 0}, 2))
		t.Log(GridLine(Vector2[int]{0, 0}, Vector2[int]{-1, -1}, 5))

		t.Log(GridLine(Vector2[int]{0, 0}, Vector2[int]{0, 1}, 1))
		t.Log(GridLine(Vector2[int]{0, 0}, Vector2[int]{0, 4}, 2))
		t.Log(GridLine(Vector2[int]{0, 0}, Vector2[int]{0, 4}, 5))
	}
}
