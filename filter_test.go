package math

import "testing"

func TestFilter(t *testing.T) {
	filter := [][]float64{
		{0.0625, 0.1250, 0.0625},
		{0.1250, 0.2500, 0.1250},
		{0.0625, 0.1250, 0.0625},
	}

	FilterInPlace(filter, filter, Vector2[int]{1, 1})

	if filter[1][1] != 0.140625 {
		t.Error(filter[1][1])
	}

	filter[1][1] = 0.25

	FilterInPlace(filter, filter, Vector2[int]{0, 0})
	FilterInPlace(filter, filter, Vector2[int]{2, 2})

	if filter[0][0] != 0.0625 || filter[2][2] != 0.0625 {
		t.Error(filter[0][0], filter[2][2])
	}

	err := FilterInPlace(filter[:2][:2], filter, Vector2[int]{1, 1})
	if err == nil {
		t.Error("wanted error", err)
	}
}
