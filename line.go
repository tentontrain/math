package math

func plotLineLow(p0, p1 Vector2[int]) []Vector2[int] {
	x0 := p0.X
	x1 := p1.X
	y0 := p0.Y
	y1 := p1.Y

	dx := x1 - x0
	dy := y1 - y0
	yi := 1
	if dy < 0 {
		yi = -1
		dy = -dy
	}
	D := 2*dy - dx
	y := y0

	points := []Vector2[int]{}

	for x := x0; x <= x1; x++ {
		points = append(points, Vector2[int]{x, y})
		if D > 0 {
			y = y + yi
			D = D - 2*dx
		}
		D = D + 2*dy
	}

	return points
}

func plotLineHigh(p0, p1 Vector2[int]) []Vector2[int] {

	x0 := p0.X
	x1 := p1.X
	y0 := p0.Y
	y1 := p1.Y

	dx := x1 - x0
	dy := y1 - y0
	xi := 1
	if dx < 0 {
		xi = -1
		dx = -dx
	}
	D := 2*dx - dy
	x := x0

	points := []Vector2[int]{}

	for y := y0; y <= y1; y++ {
		points = append(points, Vector2[int]{x, y})
		if D > 0 {
			x = x + xi
			D = D - 2*dy
		}
		D = D + 2*dx
	}

	return points
}

// Line implements Bresenham's line algorithm (translated pseudocode from wikipedia)
func Line(p0, p1 Vector2[int]) []Vector2[int] {
	x0 := p0.X
	x1 := p1.X
	y0 := p0.Y
	y1 := p1.Y

	points := []Vector2[int]{}

	if Abs(y1-y0) < Abs(x1-x0) {
		if x0 > x1 {
			points = plotLineLow(p1, p0)
			reverse(points)
		} else {
			points = plotLineLow(p0, p1)
		}
	} else {
		if y0 > y1 {
			points = plotLineHigh(p1, p0)
			reverse(points)
		} else {
			points = plotLineHigh(p0, p1)
		}
	}

	return points
}

// reverse order of a point list
func reverse(points []Vector2[int]) {
	lp := len(points) - 1
	for i := 0; i < len(points)/2; i++ {
		points[i], points[lp-i] = points[lp-i], points[i]
	}
}

// Nikolas' symetric line
func SymLine(p1, p2 Vector2[int]) []Vector2[int] {

	// move line to origin
	tr := p1
	p1 = Vector2[int]{0, 0}
	p2 = p2.Sub(tr)

	D := p2

	points := []Vector2[int]{}

	if D.X == 0 && D.Y == 0 {
		return []Vector2[int]{p1}
	}

	// special cases for straight lines
	if D.X == 0 {
		dy := D.Y / Abs(D.Y) // sign
		for i := 0; i <= Abs(D.Y); i++ {
			points = append(points, Vector2[int]{p1.X, p1.Y + dy*i}.Add(tr))
		}
		return points
	}

	if D.Y == 0 {
		dx := D.X / Abs(D.X) // sign
		for i := 0; i <= Abs(D.X); i++ {
			points = append(points, Vector2[int]{p1.X + dx*i, p1.Y}.Add(tr))
		}
		return points
	}

	// movement options depending on quadrant
	dirs := make([]Vector2[int], 3)

	dirs[0] = Vector2[int]{D.X / Abs(D.X), 0}
	dirs[1] = Vector2[int]{0, D.Y / Abs(D.Y)}
	dirs[2] = dirs[0].Add(dirs[1])

	d := p1
	points = append(points, p1.Add(tr))

	dt := make([]Vector2[int], 3)
	dxDy := make([]int, 3)
	dyDx := make([]int, 3)

	for d != D {

		minSlope := 1000000000
		bestDir := 0

		for i := 0; i < 3; i++ {
			dt[i] = d.Add(dirs[i])
			dxDy[i] = dt[i].X * D.Y
			dyDx[i] = dt[i].Y * D.X
			diff := Abs(dxDy[i] - dyDx[i])
			if diff < minSlope {
				minSlope = diff
				bestDir = i
			}
		}

		d = d.Add(dirs[bestDir])
		points = append(points, d.Add(tr))
	}

	return points
}

// Calculate the cells to be filed for a grid line (Bresenham's line algorithm)
// The line can be restricted to a specific length.
func GridLine(p1, p2 Vector2[int], length int) []Vector2[int] {

	dx := p2.X - p1.X
	dy := p2.Y - p1.Y

	high := false
	if Abs(dx) < Abs(dy) {
		high = true
		p1 = Vector2[int]{p1.Y, p1.X}
		p2 = Vector2[int]{p2.Y, p2.X}
	}

	reverse := false
	if p1.X > p2.X {
		reverse = true
		p1, p2 = p2, p1
	}

	dx = p2.X - p1.X
	dy = p2.Y - p1.Y

	// increment or decrement
	yi := 1
	if dy < 0 {
		yi = -1
		dy = -dy
	}

	points := make([]Vector2[int], Min(length, Max(dx, dy)+1))

	D := 2*dy - dx
	y := p1.Y

	for i := 0; i <= dx && i < length; i++ {
		x := p1.X + i
		idx := i
		if reverse {
			idx = len(points) - i - 1
		}
		if high {
			points[idx] = Vector2[int]{y, x}
		} else {
			points[idx] = Vector2[int]{x, y}
		}
		if D > 0 {
			y = y + yi
			D = D - 2*dx
		}
		D = D + 2*dy
	}

	return points
}
