package math

import (
	"math"
	"testing"
)

func TestMinMax(t *testing.T) {
	testsInt := []struct {
		a, b, min, max int
	}{
		{1, 2, 1, 2},
		{-101, 2, -101, 2},
		{1, 0, 0, 1},
		{math.MaxInt, math.MinInt, math.MinInt, math.MaxInt},
	}

	for _, v := range testsInt {
		if Max(v.a, v.b) != v.max {
			t.Error(v)
		}
		if Min(v.a, v.b) != v.min {
			t.Error(v)
		}
	}

	testsFloat := []struct {
		a, b, min, max float64
	}{
		{1.2, 2, 1.2, 2},
		{-101, 2, -101, 2},
		{1, 0, 0, 1},
		{math.MaxFloat64, math.MinInt, math.MinInt, math.MaxFloat64},
	}

	for _, v := range testsFloat {
		if Max(v.a, v.b) != v.max {
			t.Error(v)
		}
		if Min(v.a, v.b) != v.min {
			t.Error(v)
		}
	}
}

func TestBound(t *testing.T) {
	if Bound(3, -1, 10) != 3 {
		t.Error()
	}
	if Bound(-1.3, 0.0, 10.0) != 0 {
		t.Error()
	}
	if Bound(-1.3, -10.0, -4.0) != -4.0 {
		t.Error()
	}
}

func TestEquals(t *testing.T) {
	if !Equals(1.009, 1.0, 0.01) {
		t.Error()
	}
	if Equals(1.019, 1.0, 0.01) {
		t.Error()
	}
	if !Equals(-1.0, -1.0, 0.000000001) {
		t.Error()
	}
	if !Equals(-1.0, -1.0001, 0.001) {
		t.Error()
	}
	if Equals(-1.0, 1.0, 0.000000001) {
		t.Error()
	}
	if Equals(-1, 1, 0) {
		t.Error()
	}
	if !Equals(-6, -6, 0) {
		t.Error()
	}
}
