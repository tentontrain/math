package math

import (
	"fmt"
	"testing"
)

func TestNoise1D(t *testing.T) {
	n := Noise1D{}
	n.Initialize(10, 3)

	if testing.Verbose() {
		fmt.Println("samples: \n", n)
	}

	if n.Sample(0) != n.Sample(1) {
		t.Error(n.Sample(0), n.Sample(1), n)
	}

	if n.Sample(1.1) != n.Sample(0.1) {
		t.Error(n.Sample(1.1), n.Sample(0.1), n)
	}

	m := Noise1D{}
	m.Initialize(10, 3)
	s := m.Sample(0.7)
	m.Combine(&n)

	if n.Sample(0.7)+s != m.Sample(0.7) {
		t.Error(n.Sample(0.7), s, m.Sample(0.7))
	}
}
