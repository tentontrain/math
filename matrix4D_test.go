package math

import "testing"

func TestMatrix4fMulPoint3f(t *testing.T) {

	m := Matrix4[float32]{}
	m.Populate([16]float32{
		1, 0, 0, 2,
		0, 1, 0, -3,
		0, 0, 1, 4,
		0, 0, 0, 1,
	})
	p := Vector3[float32]{1, 2, 3}
	newp := m.MulVector(p)
	pn := Vector3[float32]{3, -1, 7}
	if newp != pn {
		t.Error(newp, pn)
	}

	m.Translation(Vector3[float32]{1, 2, -1})
	p = Vector3[float32]{1, 2, 3}
	newp = m.MulVector(p)
	pn = Vector3[float32]{2, 4, 2}
	if newp != pn {
		t.Error(newp, pn)
	}

	m.Scaling(Vector3[float32]{1, 2, 3})
	p = Vector3[float32]{1, 2, 3}
	newp = m.MulVector(p)
	pn = Vector3[float32]{1, 4, 9}
	if newp != pn {
		t.Error(newp, pn)
	}
}

func TestMatrix4fMulMatrix(t *testing.T) {
	ma, mb := Matrix4[float32]{}, Matrix4[float32]{}

	for i := range ma.Elements {
		ma.Elements[i] = float32(i)
	}

	mb.Identity()

	result := ma.MulMatrix(mb)

	for i := range ma.Elements {
		if ma.Elements[i] != result.Elements[i] {
			t.Error(result)
		}
	}
}
