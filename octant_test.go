package math

import "testing"

func TestVectorToOctant(t *testing.T) {

	type VectorInput struct {
		v Vector2[float64]
		o int
	}

	tests := []VectorInput{
		{Vector2[float64]{0, -1}, 6},
		{Vector2[float64]{-1, -1}, 5},
		{Vector2[float64]{1, 1}, 1},
		{Vector2[float64]{0, 2.3}, 2},
		{Vector2[float64]{1, 2.3}, 1},
		{Vector2[float64]{-3, -200}, 6},
	}

	for i := range tests {
		vo := VectorToOctant(tests[i].v)
		if tests[i].o != vo {
			t.Error(tests[i].v, tests[i].o, vo)
		}
	}
}
