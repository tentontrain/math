package math

import (
	"testing"
)

func TestDistancePointPoint(t *testing.T) {
	tests := []struct {
		a, b Vector2[float32]
		res  float64
	}{
		{Vector2[float32]{1, 2}, Vector2[float32]{3, 4}, 2.82842712475},
		{Vector2[float32]{3, 2}, Vector2[float32]{-3, -2}, 7.21110255093},
		{Vector2[float32]{199, -999}, Vector2[float32]{0, 1}, 1019.60825811},
	}

	for i := range tests {
		d := DistancePointPoint(tests[i].a, tests[i].b)
		if !Equals(float64(d), tests[i].res, 0.0001) {
			t.Error(tests[i], d)
		}
	}
}

func TestPointInRange(t *testing.T) {
	tests := []struct {
		p1, p2 Vector2[int]
		r      int
		yes    bool
	}{
		{Vector2[int]{1, 2}, Vector2[int]{3, 5}, 4, true},
		{Vector2[int]{1, 2}, Vector2[int]{3, 5}, 3, false},
		{Vector2[int]{-1, -2}, Vector2[int]{-3, 5}, 7, false},
		{Vector2[int]{-1, -2}, Vector2[int]{-3, 5}, 8, true},
	}

	for _, v := range tests {
		if InRange(v.p1, v.p2, v.r) != v.yes {
			t.Error(v, DistancePointPoint(v.p1, v.p2))
		}
	}
}

func TestPointLineDistance(t *testing.T) {

	type LinePoint struct {
		l1, l2, p Vector2[float64]
		result    float64
	}

	tests := []LinePoint{
		{Vector2[float64]{0, 2}, Vector2[float64]{1, 3}, Vector2[float64]{3, 2}, 2.1232},
		{Vector2[float64]{1, 0}, Vector2[float64]{2, 1}, Vector2[float64]{5, 6}, 1.4142},
	}

	for i := range tests {
		d := DistancePointLine(tests[i].l1, tests[i].l2, tests[i].p)
		if !Equals(d, tests[i].result, 0.01) {
			t.Error("Got:", d, " wanted:", tests[i].result)
		}
	}
}

func TestVectorPointLine(t *testing.T) {

	type LinePoint struct {
		l1, l2, p Vector2[float64]
		result    float64
	}

	tests := []LinePoint{
		{Vector2[float64]{0, 2}, Vector2[float64]{1, 3}, Vector2[float64]{3, 2}, 2.1213},
		{Vector2[float64]{1, 0}, Vector2[float64]{2, 1}, Vector2[float64]{5, 6}, 1.4142},
	}

	for i := range tests {
		l1 := tests[i].l1
		l2 := tests[i].l2
		p := tests[i].p
		d := DistancePointLine(l1, l2, p)
		v := VectorPointLine(l1, l2, p)
		vm := v.Length()
		if !Equals(d, tests[i].result, 0.001) {
			t.Error(d, tests[i].result)
		}
		if !Equals(d, vm, 0.001) {
			t.Error(d, vm, v)
		}
	}
}

func TestDistancePointLineSeg(t *testing.T) {

	type test struct {
		n         int
		l1, l2, p Vector2[float64]
		d         float64
	}

	tests := []test{
		{1, Vector2[float64]{1, 1}, Vector2[float64]{3, 3}, Vector2[float64]{2, 0}, 1.4142},
		{2, Vector2[float64]{1, 1}, Vector2[float64]{3, 3}, Vector2[float64]{1, 0}, 1},
		{3, Vector2[float64]{1, 1}, Vector2[float64]{3, 3}, Vector2[float64]{3, 4}, 1},
		{4, Vector2[float64]{3, 1}, Vector2[float64]{-1, 1}, Vector2[float64]{4, 1}, 1},
		{5, Vector2[float64]{3, 1}, Vector2[float64]{-1, 1}, Vector2[float64]{-1, 2}, 1},
		{6, Vector2[float64]{3, 1}, Vector2[float64]{-1, 1}, Vector2[float64]{-2, 0}, 1.4142},
	}

	for _, v := range tests {
		d := DistancePointLineSeg(v.l1, v.l2, v.p)
		if !Equals(d, v.d, 0.001) {
			t.Error(v.n, ". ", d, "!=", v.d)
		}
	}
}
