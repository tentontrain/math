package math

import "testing"

func TestDiagonal(t *testing.T) {
	tests := []struct {
		p1, p2 Vector2[int]
		yes    bool
	}{
		{Vector2[int]{0, 0}, Vector2[int]{0, 1}, false},
		{Vector2[int]{0, 0}, Vector2[int]{1, 1}, true},
		{Vector2[int]{0, 0}, Vector2[int]{-1, 1}, true},
		{Vector2[int]{-2, 3}, Vector2[int]{-1, 1}, false},
		{Vector2[int]{-2, 3}, Vector2[int]{-1, 2}, true},
		{Vector2[int]{5, 5}, Vector2[int]{166, 166}, true},
	}

	for _, v := range tests {
		if Diagonal(v.p1, v.p2) != v.yes {
			t.Error("diagonal ", v)
		}
	}
}

func TestScaleChess(t *testing.T) {
	tests := []struct {
		p1, p2 Vector2[int]
		scale  int
	}{
		{Vector2[int]{0, 0}, Vector2[int]{0, 0}, -3},
		{Vector2[int]{2, 1}, Vector2[int]{2, 1}, 2},
		{Vector2[int]{2, 1}, Vector2[int]{3, 1}, 3},
		{Vector2[int]{1, 2}, Vector2[int]{1, 3}, 3},
		{Vector2[int]{2, 1}, Vector2[int]{4, 2}, 4},
		{Vector2[int]{-3, -3}, Vector2[int]{-4, -4}, 4},
		{Vector2[int]{-3, -3}, Vector2[int]{4, 4}, -4},
		{Vector2[int]{-3, 0}, Vector2[int]{-7, 0}, 7},
	}

	for _, v := range tests {
		p := ScaleChess(v.p1, v.scale)
		if p != v.p2 {
			t.Error(v)
		}
	}
}
