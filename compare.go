package math

func Max[N Number](a, b N) N {
	if a > b {
		return a
	}
	return b
}

func Min[N Number](a, b N) N {
	if a < b {
		return a
	}
	return b
}

// Return a if a in [low,high], low if a<low and high if a>high
func Bound[N Number](a, low, high N) N {
	return Max(Min(a, high), low)
}

// Returns true if a==b with error margin e
func Equals[N Number](a, b, e N) bool {
	if (a-b) <= e && (b-a) <= e {
		return true
	}
	return false
}
